FROM maven:3.6.3-openjdk-14 as MAVEN_BUILD

COPY pom.xml /build/
COPY src /build/src

WORKDIR /build/
RUN mvn package


FROM tutum/glassfish

#WORKDIR /opt/glassfish4/glassfish/domains/domain1/autodeploy
#ENV dbDrive=com.mysql.jdbc.Driver dbConnectionUrl=jdbc:mysql://mysql:3306/opt_db dbUserName=root dbPassword=root
COPY mysql-connector-java-5.1.47.jar /opt/glassfish4/glassfish/domains/domain1/lib/ext/mysql-connector-java-5.1.47.jar
COPY mysql-connector-java-5.1.47.jar /opt/glassfish4/glassfish/domains/lib/mysql-connector-java-5.1.47.jar
COPY mysql-connector-java-5.1.47.jar /opt/glassfish4/glassfish/lib/mysql-connector-java-5.1.47.jar
COPY --from=MAVEN_BUILD /build/target/BEService-1.0.war /opt/glassfish4/glassfish/domains/domain1/autodeploy/BEService.war