package com.lyonii.beservice;

import com.lyonii.beservice.Term;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2015-08-19T21:47:18")
@StaticMetamodel(Domain.class)
public class Domain_ { 

    public static volatile SingularAttribute<Domain, String> name;
    public static volatile SingularAttribute<Domain, String> description;
    public static volatile SingularAttribute<Domain, Integer> id;
    public static volatile CollectionAttribute<Domain, Term> termCollection;

}