package com.lyonii.beservice;

import com.lyonii.beservice.Term;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2015-08-19T21:47:18")
@StaticMetamodel(Translate.class)
public class Translate_ { 

    public static volatile SingularAttribute<Translate, String> country;
    public static volatile SingularAttribute<Translate, Term> ar;
    public static volatile SingularAttribute<Translate, Term> en;
    public static volatile SingularAttribute<Translate, String> comment;
    public static volatile SingularAttribute<Translate, Integer> id;
    public static volatile SingularAttribute<Translate, String> source;
    public static volatile SingularAttribute<Translate, Term> fr;

}