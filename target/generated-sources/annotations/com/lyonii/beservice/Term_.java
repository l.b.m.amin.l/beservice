package com.lyonii.beservice;

import com.lyonii.beservice.Domain;
import com.lyonii.beservice.Term;
import com.lyonii.beservice.Translate;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2015-08-19T21:47:18")
@StaticMetamodel(Term.class)
public class Term_ { 

    public static volatile SingularAttribute<Term, String> stype;
    public static volatile SingularAttribute<Term, String> langue;
    public static volatile SingularAttribute<Term, Term> source;
    public static volatile SingularAttribute<Term, String> clean;
    public static volatile SingularAttribute<Term, String> type;
    public static volatile SingularAttribute<Term, String> structure;
    public static volatile CollectionAttribute<Term, Translate> translateFr;
    public static volatile CollectionAttribute<Term, Translate> translateAr;
    public static volatile SingularAttribute<Term, Domain> domain;
    public static volatile SingularAttribute<Term, Integer> id;
    public static volatile SingularAttribute<Term, String> state;
    public static volatile SingularAttribute<Term, String> value;
    public static volatile CollectionAttribute<Term, Term> termCollection;
    public static volatile CollectionAttribute<Term, Translate> translateEn;

}