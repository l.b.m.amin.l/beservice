/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lyonii.beservice;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author MedAmin
 */
@Entity
@Table(name = "term")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Term.findAll", query = "SELECT t FROM Term t"),
    @NamedQuery(name = "Term.findById", query = "SELECT t FROM Term t WHERE t.id = :id"),
    @NamedQuery(name = "Term.findByLangue", query = "SELECT t FROM Term t WHERE t.langue = :langue"),
    @NamedQuery(name = "Term.findByValue", query = "SELECT t FROM Term t WHERE t.value = :value"),
    @NamedQuery(name = "Term.findByClean", query = "SELECT t FROM Term t WHERE t.clean = :clean"),
    @NamedQuery(name = "Term.findByState", query = "SELECT t FROM Term t WHERE t.state = :state"),
    @NamedQuery(name = "Term.findByType", query = "SELECT t FROM Term t WHERE t.type = :type"),
    @NamedQuery(name = "Term.findByStype", query = "SELECT t FROM Term t WHERE t.stype = :stype"),
    @NamedQuery(name = "Term.findByStructure", query = "SELECT t FROM Term t WHERE t.structure = :structure")})
public class Term implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Size(max = 9)
    @Column(name = "langue")
    private String langue;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 150)
    @Column(name = "value")
    private String value;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 150)
    @Column(name = "clean")
    private String clean;
    @Size(max = 2)
    @Column(name = "state")
    private String state;
    @Size(max = 45)
    @Column(name = "type")
    private String type;
    @Size(max = 45)
    @Column(name = "stype")
    private String stype;
    @Size(max = 45)
    @Column(name = "structure")
    private String structure;
    @JoinColumn(name = "domain", referencedColumnName = "id")
    @ManyToOne
    private Domain domain;
    @OneToMany(mappedBy = "source")
    private Collection<Term> termCollection;
    @JoinColumn(name = "source", referencedColumnName = "id")
    @ManyToOne
    private Term source;
    @OneToMany(mappedBy = "ar")
    private Collection<Translate> translateAr;
    @OneToMany(mappedBy = "en")
    private Collection<Translate> translateEn;
    @OneToMany(mappedBy = "fr")
    private Collection<Translate> translateFr;

    public Term() {
    }

    public Term(Integer id) {
        this.id = id;
    }

    public Term(Integer id, String value, String clean) {
        this.id = id;
        this.value = value;
        this.clean = clean;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLangue() {
        return langue;
    }

    public void setLangue(String langue) {
        this.langue = langue;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getClean() {
        return clean;
    }

    public void setClean(String clean) {
        this.clean = clean;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStype() {
        return stype;
    }

    public void setStype(String stype) {
        this.stype = stype;
    }

    public String getStructure() {
        return structure;
    }

    public void setStructure(String structure) {
        this.structure = structure;
    }

    public Domain getDomain() {
        return domain;
    }

    public void setDomain(Domain domain) {
        this.domain = domain;
    }

    @XmlTransient
    public Collection<Term> getTermCollection() {
        return termCollection;
    }

    public void setTermCollection(Collection<Term> termCollection) {
        this.termCollection = termCollection;
    }

    public Term getSource() {
        return source;
    }

    public void setSource(Term source) {
        this.source = source;
    }

    @XmlTransient
    public Collection<Translate> getTranslateAr() {
        return translateAr;
    }

    public void setTranslateAr(Collection<Translate> translateCollection) {
        this.translateAr = translateCollection;
    }

    @XmlTransient
    public Collection<Translate> getTranslateEn() {
        return translateEn;
    }

    public void setTranslateEn(Collection<Translate> translateCollection1) {
        this.translateEn = translateCollection1;
    }

    @XmlTransient
    public Collection<Translate> getTranslateFr() {
        return translateFr;
    }

    public void setTranslateFr(Collection<Translate> translateCollection2) {
        this.translateFr = translateCollection2;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Term)) {
            return false;
        }
        Term other = (Term) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lyonii.beservice.Term[ id=" + id + " ]";
    }
    
}
