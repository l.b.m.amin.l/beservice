/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lyonii.beservice;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author MedAmin
 */
@Entity
@Table(name = "translate")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Translate.findAll", query = "SELECT t FROM Translate t"),
    @NamedQuery(name = "Translate.findById", query = "SELECT t FROM Translate t WHERE t.id = :id"),
    @NamedQuery(name = "Translate.findBySource", query = "SELECT t FROM Translate t WHERE t.source = :source"),
    @NamedQuery(name = "Translate.findByCountry", query = "SELECT t FROM Translate t WHERE t.country = :country"),
    @NamedQuery(name = "Translate.findByComment", query = "SELECT t FROM Translate t WHERE t.comment = :comment")})
public class Translate implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Size(max = 250)
    @Column(name = "source")
    private String source;
    @Size(max = 250)
    @Column(name = "country")
    private String country;
    @Size(max = 250)
    @Column(name = "comment")
    private String comment;
    @JoinColumn(name = "ar", referencedColumnName = "id")
    @ManyToOne
    private Term ar;
    @JoinColumn(name = "en", referencedColumnName = "id")
    @ManyToOne
    private Term en;
    @JoinColumn(name = "fr", referencedColumnName = "id")
    @ManyToOne
    private Term fr;

    public Translate() {
    }

    public Translate(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Term getAr() {
        return ar;
    }

    public void setAr(Term ar) {
        this.ar = ar;
    }

    public Term getEn() {
        return en;
    }

    public void setEn(Term en) {
        this.en = en;
    }

    public Term getFr() {
        return fr;
    }

    public void setFr(Term fr) {
        this.fr = fr;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Translate)) {
            return false;
        }
        Translate other = (Translate) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.lyonii.beservice.Translate[ id=" + id + " ]";
    }
    
}
