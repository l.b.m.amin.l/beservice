/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lyonii.beservice;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

/**
 *
 * @author MedAmin
 */
public class resquet implements Serializable {
    
    private Term t;
    private Collection<Term> tc;
    private Collection<Translate> tr;
    private ArrayList<String> wr;
    private String type;
    private String ext;
    private String text;
    private String url;

    public resquet() {
    }

    public Term getT() {
        return this.t;
    }

    public void setT(Term t) {
        this.t = t;
    }

    public Collection<Term> getTc() {
        return this.tc;
    }

    public void setTc(Collection<Term> tc) {
        this.tc = tc;
    }

    public Collection<Translate> getTr() {
        return this.tr;
    }

    public void setTr(Collection<Translate> tr) {
        this.tr = tr;
    }

    public ArrayList<String> getWr() {
        return this.wr;
    }

    public void setWr(ArrayList<String> wr) {
        this.wr = wr;
    }

    public String getType() {
        return this.type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getExt() {
        return this.ext;
    }

    public void setExt(String ext) {
        this.ext = ext;
    }

    public String getText() {
        return this.text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getUrl() {
        return this.url;
    }

    public void setUrl(String url) {
        this.url = url;
    }


    
    
    
}
