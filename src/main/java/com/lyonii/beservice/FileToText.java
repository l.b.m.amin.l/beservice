/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lyonii.beservice;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Base64;
import org.apache.pdfbox.cos.COSDocument;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.util.PDFTextStripper;
import org.apache.poi.hwpf.extractor.WordExtractor;
import org.apache.poi.xwpf.extractor.XWPFWordExtractor;
import org.apache.poi.xwpf.usermodel.XWPFDocument;

/**
 *
 * @author MedAmin
 */
public class FileToText {
    
    public String FileExtractor(String data, String file) {

        try {
            byte[] decodedBytes = Base64.getDecoder().decode(data);
            InputStream is = new ByteArrayInputStream(decodedBytes);

            if (file.endsWith(".doc") || file.endsWith(".DOC")) {

                return DocExtract(is);
            }
            else if (file.endsWith(".docx") || file.endsWith(".DOCX")) {

                return DocxExtract(is);
            }
            else if (file.endsWith(".txt") || file.endsWith(".TXT")) {

                return TxtExtract(is);
            }
            else if (file.endsWith(".pdf") || file.endsWith(".PDF")) {

                return PdfExtract(is);
            } else {
                return "Format not suported";
            }
        } catch (Exception ex) {
            return "Error";
        }

    }

    public String DocExtract(InputStream file) {

        try {
            WordExtractor extractor = new WordExtractor(file);
            String text = "";
            for (String rawText : extractor.getParagraphText()) {
                text += extractor.stripFields(rawText);

            }
            return text.replaceAll("[^A-Za-z\\u0600-\\u06FF0-9. ]+", " ");
        } catch (Exception ex) {
            return "Error";
        }

    }

    public String DocxExtract(InputStream file) {

        try {

            XWPFDocument docx = new XWPFDocument(file);
            //using XWPFWordExtractor Class
            XWPFWordExtractor we = new XWPFWordExtractor(docx);
            return we.getText().replaceAll("[^A-Za-z\\u0600-\\u06FF0-9. ]+", " ");
        } catch (Exception ex) {
            return "Error";
        }
    }

    public String TxtExtract(InputStream file) {

        try {
            //File ff = new File(file);
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(file, "UTF8"));
            String t = "";
            String r = "";
            while ((r = in.readLine()) != null) {
                t += r;
            }
            in.close();
            return t.replaceAll("[^A-Za-z\\u0600-\\u06FF0-9. ]+", " ");
        } catch (Exception ex) {
            return "Error";
        }

    }

    public String PdfExtract(InputStream file) {

        PDFParser parser = null;
        PDDocument pdDoc = null;
        COSDocument cosDoc = null;
        PDFTextStripper pdfStripper;

        String parsedText;
        try {
            parser = new PDFParser(file);
            parser.parse();
            cosDoc = parser.getDocument();
            pdfStripper = new PDFTextStripper();
            pdDoc = new PDDocument(cosDoc);
            parsedText = pdfStripper.getText(pdDoc);
            return parsedText.replaceAll("[^A-Za-z\\u0600-\\u06FF0-9. ]+", " ");

        } catch (Exception ex) {
            return "Error";
        }

    }
}
