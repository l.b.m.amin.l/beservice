/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lyonii.beservice.service;

import com.lyonii.beservice.FileToText;
import com.lyonii.beservice.Term;
import com.lyonii.beservice.Translate;
import com.lyonii.beservice.resquet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;


/**
 *
 * @author MedAmin
 */
@Stateless
@Path("term")
public class TermFacadeREST extends AbstractFacade<Term> {
    @PersistenceContext(unitName = "com.lyonii_BEService_war_1.0PU")
    private EntityManager em;

    public TermFacadeREST() {
        super(Term.class);
    }

    @POST
    @Produces({"application/xml", "application/json"})
    @Consumes({"application/xml", "application/json"})
    public Term createe(Term entity) {
        entity.setClean(CleanString(entity.getValue()));
        super.create(entity);
        return entity;
    }
   //======================================================

    @POST 
    @Path("file")
    @Produces({"application/xml", "application/json"})
    @Consumes({"application/xml", "application/json"})
    public resquet dd(resquet r) { 
        
        
        //== convert file to String ==// 
        FileToText f = new FileToText();
        String in = f.FileExtractor(r.getText(), r.getUrl());
        //===========================//
        
        //== list of words connected to the term ==//
        Term t = super.find(r.getT().getId());
        Term s = t.getSource();
        if (s == null) {
            s = t;
        }
        Collection<Term> ct = s.getTermCollection();
        ArrayList<Translate> ctr = new ArrayList<Translate>();
        ctr.addAll(getTranslate(s));
        for (Term ta : ct) {
            ctr.addAll(getTranslate(ta));
        }
        ArrayList<String> words = new ArrayList<String>();
        for (Translate tra : ctr) {
            if (tra.getAr() != null) {
                words.add(tra.getAr().getValue());
                words.add(tra.getAr().getClean());
            }
            if (tra.getFr() != null) {
                words.add(tra.getFr().getValue());
                words.add(tra.getFr().getClean());
            }
            if (tra.getEn() != null) {
                words.add(tra.getEn().getValue());
                words.add(tra.getEn().getClean());
            }

        }
        Set<String> hs = new HashSet<>();
        hs.addAll(words);
        words.clear();
        words.addAll(hs);
        //=============================================//
        
        //== Response ================================//
        resquet ans = new resquet();
        ans.setText(in);
        ans.setWr(words);
        ans.setTc(ct);
        ans.setTr(ctr);
        //============================================//
        return ans; 
    }
          
    //=======================================================
    @POST
    @Path("{id}")
    @Consumes({"application/xml", "application/json"})
    public void edit(@PathParam("id") Integer id, Term entity) {
       
        entity.setClean(CleanString(entity.getValue()) ); 
        super.edit(entity);
    }

   
    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") Integer id) {
        super.remove(super.find(id));
    }

    @GET
    @Path("{id}")
    @Produces({"application/xml", "application/json"})
    public Term find(@PathParam("id") Integer id) {
        return super.find(id);
    }
    
    @GET
    @Path("translate/{id}")
    @Produces({"application/xml", "application/json"})
    public Collection<Translate> translate(@PathParam("id") Integer id) {
        Term t = super.find(id);
        switch (t.getLangue()) {
            case "Français":
                return t.getTranslateFr();
            case "English":
                return t.getTranslateEn();
            default:
                return t.getTranslateAr();
        }

    }
       
    @GET
    @Path("search/{value}")
    @Produces({"application/xml", "application/json"})
    public List<Term> search(@PathParam("value") String value) {
        String clean = CleanString(value);
        String tclean = clean;
        if (tclean.startsWith("ال")) {
            tclean = tclean.replaceFirst("ال", "");
        }

        return em.createQuery(
                "SELECT t FROM Term t WHERE"
                + " t.value LIKE :value or"
                + " t.clean LIKE :value or"
                + " t.clean LIKE :clean or"
                + " t.clean LIKE :tclean")
                .setParameter("value", "%" + value + "%")
                .setParameter("clean", "%" + clean + "%")
                .setParameter("tclean", "%" + tclean + "%")
                .getResultList();
    }


    @GET
    @Override
    @Produces({"application/xml", "application/json"})
    public List<Term> findAll() {
        return super.findAll();
    }

    @GET
    @Path("{from}/{to}")
    @Produces({"application/xml", "application/json"})
    public List<Term> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces("text/plain")
    public String countREST() {
        return String.valueOf(super.count());
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    public String CleanString(String value) {

        String clean = value;
        clean = clean.replace("لأ", "لا");
        clean = clean.replace("لإ", "لا");
        clean = clean.replace("لآ", "لا");
        clean = clean.replace("أ", "ا");
        clean = clean.replace("إ", "ا");
        clean = clean.replace("آ", "ا");
        clean = clean.replace("ٌ", "");
        clean = clean.replace("ُ", "");
        clean = clean.replace("ً", "");
        clean = clean.replace("َ", "");
        clean = clean.replace("ٍ", "");
        clean = clean.replace("ِ", "");
        clean = clean.replace("ْ", "");
        clean = clean.replace("ّ", "");

        return clean;
    }
      
    public Collection<Translate> getTranslate(Term t) {

        switch (t.getLangue()) {
            case "Français":
                return t.getTranslateFr();
            case "English":
                return t.getTranslateEn();
            default:
                return t.getTranslateAr();
        }

    } 
    
    
    
    
}
