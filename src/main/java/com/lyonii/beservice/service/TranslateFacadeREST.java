/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lyonii.beservice.service;

import com.lyonii.beservice.Term;
import com.lyonii.beservice.Translate;
import java.util.Collection;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

/**
 *
 * @author MedAmin
 */
@Stateless
@Path("translate")
public class TranslateFacadeREST extends AbstractFacade<Translate> {
    @PersistenceContext(unitName = "com.lyonii_BEService_war_1.0PU")
    private EntityManager em;

    public TranslateFacadeREST() {
        super(Translate.class);
    }
    
    

    @POST
    @Produces({"application/xml", "application/json"})
    @Consumes({"application/xml", "application/json"})
    public Translate createe(Translate entity) {
               
        super.create(entity);
        return entity;
    }

    @POST
    @Path("{id}")
    @Consumes({"application/xml", "application/json"})
    public void edit(@PathParam("id") Integer id, Translate entity) {
        super.edit(entity);
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") Integer id) {
        super.remove(super.find(id));
    }

   /* @GET
    @Path("{id}")
    @Produces({"application/xml", "application/json"})
    public Translate find(@PathParam("id") Integer id) {
        return super.find(id);
    }*/
    @GET
    @Path("{term}")
    @Produces({"application/xml", "application/json"})
    public Collection<Translate> find(@PathParam("term") String term) {
        
        
        Term t = (Term)em.createQuery(
        "SELECT t FROM Term t WHERE t.value = :value ")
        .setParameter("value", term)        
        .setMaxResults(1)
        .getSingleResult();
        
        if(t.getLangue().equals("Français"))
        return t.getTranslateFr();
        if(t.getLangue().equals("English"))
        return t.getTranslateEn();
       else return t.getTranslateAr();
    }


    @GET
    @Override
    @Produces({"application/xml", "application/json"})
    public List<Translate> findAll() {
        return super.findAll();
    }

    @GET
    @Path("{from}/{to}")
    @Produces({"application/xml", "application/json"})
    public List<Translate> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces("text/plain")
    public String countREST() {
        return String.valueOf(super.count());
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
}
